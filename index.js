const config = {
  apiKey: "AIzaSyC3kPZd7jejDiVLvlk9NncQ1Sk3uYEGfLY",
  authDomain: "bradjs-825d9.firebaseapp.com",
  databaseURL: "https://bradjs-825d9.firebaseio.com",
  projectId: "bradjs-825d9",
  storageBucket: "bradjs-825d9.appspot.com",
  messagingSenderId: "427884843868"
};
// Initialize Firebase
firebase.initializeApp(config);

// Get elements
const btnLogin = document.getElementById('btnLogin');
const btnSignUp = document.getElementById('btnSignUp');
const btnFacebookLogin = document.getElementById('btnFacebookLogin');
const debugMsg = document.getElementById('debugMsg')
const btnTestButton = document.getElementById('btnTestButton')

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser) {
    // User Logged in
    console.log("logged in!");
    window.location.replace('/home.html');
  } else {
    // User not logged in
    console.log('not logged in');
  }
});

var providerFB = new firebase.auth.FacebookAuthProvider();

function fbFunc() {
  firebase.auth().signInWithPopup(providerFB).then(function(result) {
     var token = result.credential.accessToken;
     var user = result.user;

     console.log(token)
     console.log(user)
     window.location.replace('/home.html');
  }).catch(function(error) {
     console.log(error.code);
     console.log(error.message);
  });
}
