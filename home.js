const config = {
  apiKey: "AIzaSyC3kPZd7jejDiVLvlk9NncQ1Sk3uYEGfLY",
  authDomain: "bradjs-825d9.firebaseapp.com",
  databaseURL: "https://bradjs-825d9.firebaseio.com",
  projectId: "bradjs-825d9",
  storageBucket: "bradjs-825d9.appspot.com",
  messagingSenderId: "427884843868"
};
// Initialize Firebase
firebase.initializeApp(config);

// Get elements
const btnLogout = document.getElementById('btnLogout');
const debugMsg = document.getElementById('debugMsg')

btnLogout.addEventListener('click', e => {
  firebase.auth().signOut();
});

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser) {
    // User Logged in
    console.log("logged in!");
    var userId = firebase.auth().currentUser.uid;
    var userFolder = firebase.database().ref('/users/' + userId);
    // Reads data from database
    userFolder.on('value', function(snapshot){
      try {
        var complete = snapshot.val().complete;
        if(complete == "Yes") {
          var univ = snapshot.val().university;
          var email = snapshot.val().email;
          var gender = snapshot.val().gender;
          debugMsg.innerHTML = "Name: " + firebaseUser.displayName + "<br>Email: " + email + "<br>University: " + univ + "<br>Gender: " + gender;
        }
      // Catch error: Meaning that user has NOT completed the create an account
      } catch (err){
        firebaseUser.delete().then(function() {
          // User deleted.
        }, function(error) {
          // An error happened.
        });
      }
    });
    btnLogout.style.display = "block";
    console.log(firebaseUser);
  } else {
    // User not logged in
    console.log('not logged in');
    window.location.replace('/index.html');
    btnLogout.style.display = "none";
    debugMsg.innerHTML = "Firebase User NOT Logged In!";
    // btnLogout.classList.add('hide');
  }
});
