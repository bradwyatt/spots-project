const config = {
  apiKey: "AIzaSyC3kPZd7jejDiVLvlk9NncQ1Sk3uYEGfLY",
  authDomain: "bradjs-825d9.firebaseapp.com",
  databaseURL: "https://bradjs-825d9.firebaseio.com",
  projectId: "bradjs-825d9",
  storageBucket: "bradjs-825d9.appspot.com",
  messagingSenderId: "427884843868"
};
// Initialize Firebase
firebase.initializeApp(config);

// Get elements
const name = getQueryVariable("first_name");
const email = getQueryVariable("email");
const gender = getQueryVariable("gender");
const university = getQueryVariable("university").replace(/[+]/g, ' ');
const btnFacebookLogin = document.getElementById('btnFacebookLogin');
const debugMsg = document.getElementById('debugMsg')

// Get a reference to the database service
var database = firebase.database();
var usersRef = firebase.database().ref("users/");

function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser) {
    // User Logged in
    console.log(firebaseUser)
    console.log("logged in!");
    btnFacebookLogin.style.display = "none";
    if(firebaseUser.displayName != null) {
      debugMsg.innerHTML = "Firebase User Logged In!";
    }
    var nameOfUser = firebaseUser.uid
    var usersRef = firebase.database().ref("users/" + firebaseUser.uid);
    usersRef.update({
      "email": email,
      "facebook_email": firebaseUser.email,
      "gender": gender,
      "name": firebaseUser.displayName,
      "university": university,
      "complete": "Yes"

    });
    window.location.replace('/home.html');
  } else {
    // User not logged in
    console.log('not logged in');
    btnFacebookLogin.style.display = "block";
    debugMsg.innerHTML = "Firebase User NOT Logged In!";
  }
});

var providerFB = new firebase.auth.FacebookAuthProvider();

btnFacebookLogin.addEventListener('click', e => {
 firebase.auth().signInWithPopup(providerFB).then(function(result) {
    var token = result.credential.accessToken;
    var user = result.user;
    console.log(token)
    console.log(user)
 }).catch(function(error) {
    console.log(error.code);
    console.log(error.message);
 });
});
