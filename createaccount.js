const config = {
  apiKey: "AIzaSyC3kPZd7jejDiVLvlk9NncQ1Sk3uYEGfLY",
  authDomain: "bradjs-825d9.firebaseapp.com",
  databaseURL: "https://bradjs-825d9.firebaseio.com",
  projectId: "bradjs-825d9",
  storageBucket: "bradjs-825d9.appspot.com",
  messagingSenderId: "427884843868"
};
// Initialize Firebase
firebase.initializeApp(config);

// Get elements
const txtName = document.getElementById('txtName');
const txtEmail = document.getElementById('txtEmail');
const txtUniversity = document.getElementById('txtUniversity');
const genderSelect = document.getElementById('genderSelect');
const btnLogout = document.getElementById('btnLogout');
const debugMsg = document.getElementById('debugMsg')

// Get a reference to the database service
var database = firebase.database();
var usersRef = firebase.database().ref("users/");

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser) {
    // User Logged in
    console.log(firebaseUser)
    console.log("logged in!");
    window.location.replace('/home.html');
  } else {
    // User not logged in
    console.log('not logged in');
    txtName.style.display = "block";
    txtEmail.style.display = "block";
    txtUniversity.style.display = "block";
    genderSelect.style.display = "block";
    debugMsg.innerHTML = "Firebase User NOT Logged In!";
  }
});
