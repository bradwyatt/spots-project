const config = {
  apiKey: "AIzaSyC3kPZd7jejDiVLvlk9NncQ1Sk3uYEGfLY",
  authDomain: "bradjs-825d9.firebaseapp.com",
  databaseURL: "https://bradjs-825d9.firebaseio.com",
  projectId: "bradjs-825d9",
  storageBucket: "bradjs-825d9.appspot.com",
  messagingSenderId: "427884843868"
};
// Initialize Firebase
firebase.initializeApp(config);

// Get elements
const txtName = document.getElementById('txtName');
const txtEmail = document.getElementById('txtEmail');
const txtPassword = document.getElementById('txtPassword');
const btnLogin = document.getElementById('btnLogin');
const btnSignUp = document.getElementById('btnSignUp');
const btnLogout = document.getElementById('btnLogout');
const btnFacebookLogin = document.getElementById('btnFacebookLogin');
const debugMsg = document.getElementById('debugMsg')
const btnTestButton = document.getElementById('btnTestButton')

// Add login event
btnLogin.addEventListener('click', e => {
  const email = txtEmail.value;
  const pass = txtPassword.value;
  const auth = firebase.auth();
  // Sign In
  const promise = auth.signInWithEmailAndPassword(email, pass);
  promise.catch(e => console.log(e.message));
});

// Add signup event
btnSignUp.addEventListener('click', e => {
  // CHECK FOR REAL EMAILS
  const email = txtEmail.value;
  const pass = txtPassword.value;
  const name = txtName.value;
  const auth = firebase.auth();
  // Sign In
  const promise = auth.createUserWithEmailAndPassword(email, pass);
  promise.catch(e => console.log(e.message));
  auth.onAuthStateChanged(firebaseUser => {
    if(firebaseUser) {
      console.log("inside Signup firebase");
      console.log(firebaseUser);
      firebaseUser.updateProfile({
        displayName: name
      }).then(function() {
        debugMsg.innerHTML = firebaseUser.displayName + "\n " + firebaseUser.email;
      });
    } else { }
  });
});

btnLogout.addEventListener('click', e => {
  firebase.auth().signOut();
});

// Add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser => {
  if(firebaseUser) {
    // User Logged in
    console.log("logged in!");
    btnLogout.style.display = "block";
    txtName.style.display = "none";
    txtEmail.style.display = "none";
    txtPassword.style.display = "none";
    btnLogin.style.display = "none";
    btnSignUp.style.display = "none";
    btnFacebookLogin.style.display = "none";
    if(firebaseUser.displayName != null) {
      debugMsg.innerHTML = "Username: " + firebaseUser.displayName + "<br> Email:" + firebaseUser.email;
    }
    // btnLogout.classList.remove('hide');
  } else {
    // User not logged in
    console.log('not logged in');
    btnLogout.style.display = "none";
    txtName.style.display = "block";
    txtEmail.style.display = "block";
    txtPassword.style.display = "block";
    btnLogin.style.display = "block";
    btnSignUp.style.display = "block";
    btnFacebookLogin.style.display = "block";
    debugMsg.innerHTML = "Firebase User NOT Logged In! <br> Email and Password are required to Sign Up.";
    // btnLogout.classList.add('hide');
  }
});

var providerFB = new firebase.auth.FacebookAuthProvider();

btnFacebookLogin.addEventListener('click', e => {
 firebase.auth().signInWithPopup(providerFB).then(function(result) {
    var token = result.credential.accessToken;
    var user = result.user;

    console.log(token)
    console.log(user)
 }).catch(function(error) {
    console.log(error.code);
    console.log(error.message);
 });
});

btnTestButton.addEventListener('click', e => {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      debugMsg.innerHTML = user.displayName + "\n " + user.email;
    } else {
      //None
    }
  });
});
